# EP2 - OO 2019.2 (UnB - Gama)

Turma: Carla
Data de entrega: 05/11/2019

## Descrição

É um jogo implementado na linguagem de programação java utilizando o paradigma de orientação a objeto.O jogo implementado é o jogo da cobrinha.Nesse jogo o
 jogador controla uma longa e fina criatura que se arrasta pela tela, coletando comida (ou algum outro item), não podendo colidir com seu próprio corpo ou as "paredes" que cercam a área de jogo. Cada vez que a serpente come um pedaço de comida, seu rabo cresce, aumentando a dificuldade do jogo. O usuário controla a direção da cabeça da serpente (para cima, para baixo, esquerda e direita) e seu corpo segue.


## Especificações
    * Versão do java: 11.04
    * IDE utilizada: Netbeans apache 11.2
## Tipos de Snakes
Tioos de snakes implementadas:
* **Comum:** A Snake classica, sem habilidades especiais.
* **Kitty:** Essa Snake tem as habilidades de atravessar as barreiras do jogo, mas não pode atravessar as bordas nem a si mesma.
* **Star:** Recebe o dobro de pontos ao comer as frutas.

## Como Jogar
* Selecione no menu inicial a cobra que deseja jogar
* Use as teclas direcionais para movimentar a cobra

## Colisões

As Snakes colidem com barreiras no interior do jogo, salvo a Snake Kitty que atravessa barreiras. Nenhuma cobra deve atravessar as bordas do jogo. Quando houver algum tipo de colisão, tanto nas barreiras, quanto elas mesmas, a Snake deve morrer.

## Frutas

As frutas são elementos que aparecem aleatoriamente e são os objetivos das Snakes. 

* **Simple Fruit:** Fruta comum, dá um ponto e aumenta o tamanho da cobra.
* cor da fruta:**Laranja**
* **Bomb Fruit:** Essa fruta deve levar a morte da Snake.
* cor da fruta:**Branca**
* **Big Fruit:** Dá o dobro de pontos da Simple Fruit e aumenta o tamanho da cobra da mesma forma que a Simple Fruit.
* cor da fruta:**Vermelha**
    
* **Decrease Fruit:** Diminui o tamanho da cobra para o tamanho inicial, sem fornecer nem retirar pontos.
* cor da fruta:**Azul claro**

## Pontos

Os pontos são calculados de acordo com as frutas coletadas.
* **Simple Fruit:** 1 ponto.
* **Bomb Fruit:** gameOver.
* **Big Fruit:** 2 pontos
* **Decrease Fruit:** 1 ponto
## Como executar
* através da IDE Netbeans clicando em run ou compilando com javac.
