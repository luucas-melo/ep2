/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gameplay;

import classes.ClassicSnake;
import java.awt.Dimension;
import javax.swing.BoxLayout;
import javax.swing.JFrame;

/**
 *
 * @author lucas
 */
public class MainFrame extends JFrame {
   public MainFrame(){
        setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
        //classicSnake snake = new classicSnake();
        //Board board = new Board(snake);
        setPreferredSize(new Dimension(450,450));
        setTitle("Snake");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setResizable(false);
        //add(board);
        pack();
        setLocationRelativeTo(null);
        //setVisible(true);
        
   } 
}
