package gameplay;




import classes.ClassicSnake;
import classes.SimpleFruit;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JPanel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author lucas
 */
public class Board extends JPanel{

   
    
    private static int boardHeight = 450;
    private static int boardWidth = 450;
    private ClassicSnake snake;
    private int wallHeight=15;
    //private final MainGame maingame;
    
    public void setSnake(ClassicSnake snake) {
		this.snake = snake;
    }
    
   


    public Board(ClassicSnake snake){
        this.snake = snake;
        setBackground(Color.black);
        setFocusable(true);
        setBounds(15, 15, boardHeight, boardWidth);    
        
        
    } 
    
    
  
    
    @Override
    public void paintComponent(Graphics g) {
        //bigFruit = new BigFruit();
        super.paintComponent(g);
        g.fill3DRect(205, 200, 150,wallHeight, true);
        g.fill3DRect(205, 300, 150, wallHeight, true);
        g.fill3DRect(105, 100, wallHeight, 150, true);
        g.drawImage(SimpleFruit.getFruitImage(), SimpleFruit.getFruit_x(), SimpleFruit.getFruit_y(), this);   
        for (int z = 0; z < snake.getBodySize(); z++) { 
            if (z == 0) {
                g.drawImage(snake.getHeadImage(), snake.getDX()[z], snake.getDY()[z], this);
                
            } 
            else {
                g.drawImage(snake.getBodyImage(),snake.getDX()[z], snake.getDY()[z], this);   
            }
            }

            Toolkit.getDefaultToolkit().sync();
        }       
    
     public static int getBoardHeight() {
        return boardHeight;
    }

    public static int getBoardWidth() {
        return boardWidth;
    }

    public static void setBoardHeight(int aBoardHeight) {
        boardHeight = aBoardHeight;
    }

    public static void setBoardWidth(int aBoardWidth) {
        boardWidth = aBoardWidth;
    }
     
    
}
