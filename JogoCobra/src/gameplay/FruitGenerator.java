/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gameplay;


import classes.BigFruit;
import classes.BombFruit;
import classes.ClassicSnake;
import classes.DecreaseFruit;
import classes.SimpleFruit;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lucas
 */
public class FruitGenerator implements Runnable {
    private static Random randomFruit;
    private SimpleFruit fruit;
    private static int generatedFruitID;
    private final int simpleFruit = 0;
    private final int bigFruit = 1;
    private final int decreaseFruit = 2;
    private final int bombFruit = 3;
    
   
    private static int time;
    
    public FruitGenerator(int generatedFruitID,int time){
        this.generatedFruitID = generatedFruitID;   
        this.time = time;
        randomFruit = new Random();    
        
    }
    
    @Override
    public void run() {
        while(Gameplay.isRunning()) {
            randomFruit();
            try {
                Thread.sleep(time);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    
    public void randomFruit(){
        switch(randomFruit.nextInt(4)){
			case simpleFruit:
                             //System.out.println("000");
                             generatedFruitID = simpleFruit;
                             fruit = new SimpleFruit();
                             this.time = 10000;
                         
                            break;
			case bigFruit:
                            //System.out.println("111");
                            fruit = new BigFruit();
                            generatedFruitID = bigFruit;
                            this.time = 5000;
                            break;
                        case decreaseFruit:
                            //System.out.println("222");
                            generatedFruitID = decreaseFruit;
                            fruit = new DecreaseFruit();
                            this.time = 4000;
                            break;
                        case bombFruit:
                             //System.out.println("333");
                             generatedFruitID = bombFruit;
                             fruit = new BombFruit();
                             this.time = 3000;
                             break;
         
		}
    }

    public static Random getRandom() {
        return randomFruit;
    }

    public void setRandom(Random randomFruit) {
        this.randomFruit = randomFruit;
    }

    public SimpleFruit getFruit() {
        return fruit;
    }

    public void setFruit(SimpleFruit fruit) {
        this.fruit = fruit;
    }

    public static int getGeneratedFruitID() {
        return generatedFruitID;
    }

    public void setGeneratedFruit(int generatedFruitID) {
        this.generatedFruitID = generatedFruitID;
    }

    public SimpleFruit getfruit() {
        return fruit;
    }


    public static void setTime(int atime) {
        time = atime;
    }

    public static int getTime() {
        return time;
    }
    
}
