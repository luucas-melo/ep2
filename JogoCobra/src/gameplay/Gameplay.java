package gameplay;

import classes.ClassicSnake;
import classes.KittySnake;
import classes.SimpleFruit;
import classes.StarSnake;
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JLabel;
import view.GameOver;
import view.Menu;



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author lucas
 */
public class Gameplay implements Runnable,KeyListener{
    
  private ClassicSnake snake;
  //private StarSnake snake;
  private final int DELAY = 80;
  private Thread gameLoop;
  private Thread fruitThread1;
  private Thread fruitThread2;
  private static boolean running;
  private MainFrame mainFrame;
  private static int points=0;
  private static JLabel scoreLabel;
  private Board board;
  private final int randPos = 30;
  private int fruit_x;
  private int fruit_y;
  private SimpleFruit fruit;
  private FruitGenerator fruitGenerator1;
  
 

  public Gameplay(){
      scoreLabel = new JLabel();
      scoreLabel.setText("Score: "+points);
      scoreLabel.setForeground(Color.white);
      mainFrame = new MainFrame();
      mainFrame.setVisible(true);
      initGame(Menu.getChosenSnake());
      
      board = new Board(snake);
      mainFrame.add(board);
      board.add(scoreLabel);
      board.addKeyListener(this);
      board.requestFocus();
      //mainFrame.requestFocus();
      gameLoop = new Thread(this);
      gameLoop.start();
   
      
      
      
  }
  
   
  
  public void initGame(int chosenSnake) {
		running = true;
                
                //SimpleFruit.generateFruit();
		switch(Menu.getChosenSnake()) {
			case 1:
				snake = new ClassicSnake();
				break;
			case 2:
				snake = new KittySnake();
				break;
			case 3:
                                snake = new StarSnake();
				
				break;
		}
                fruitGenerator1 = new FruitGenerator(fruitGenerator1.getGeneratedFruitID(),10000);
                fruitThread1 = new Thread(fruitGenerator1);
                fruitThread1.start();	
	}

    public void run() {
		while(running) {    
                        snake.move();
                        //generateFruit(fruitSpawner);
                        //board.add(barrier);
                        snake.checkAteFruit();
                        snake.checkCollision();
                        board.repaint();
          
			try {
				Thread.sleep(120);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
                mainFrame.setVisible(false);
                mainFrame.dispose();
                new GameOver();              
	}
   

  
  @Override
    public void keyPressed(KeyEvent e) {

            int key = e.getKeyCode();

            if ((key == KeyEvent.VK_LEFT) && (!snake.getRightDirection())) {
                snake.setLeftDirection(true);
                snake.setUpDirection(false);
                snake.setDownDirection(false);
            }

            if ((key == KeyEvent.VK_RIGHT) && (!snake.getLeftDirection())) {
                snake.setRightDirection(true);
                snake.setUpDirection(false);
                snake.setDownDirection(false); 
            }

            if ((key == KeyEvent.VK_UP) && (!snake.getDownDirection())) {
                snake.setUpDirection(true);
                snake.setRightDirection(false);
                snake.setLeftDirection(false);
            }

            if ((key == KeyEvent.VK_DOWN) && (!snake.getUpDirection())) {
                snake.setDownDirection(true);
                snake.setRightDirection(false);
                snake.setLeftDirection(false);
            }
        }

    @Override
    public void keyTyped(KeyEvent ke) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void keyReleased(KeyEvent ke) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public boolean getRunning() {
        return running;
    }

    public static void gameOver() {
        running = false;
    }


    public static int getPoints() {
        return points;
    }
    public static void setPoints(int aPoints) {
        //System.out.println(points);
        points = aPoints;
    }
    public static int increasePoints() {
        if(SimpleFruit.isBigFruit())
           return points+=2;
        return points++;     
    }

    public static JLabel getScoreLabel() {
        return scoreLabel;
    }

    public static void setScoreLabel(JLabel aScoreLabel) {
        scoreLabel = aScoreLabel;
    }

    public static boolean isRunning() {
        return running;
    }

   
    
    
}

   


 