/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import gameplay.FruitGenerator;
import gameplay.Gameplay;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.image.ImageObserver;
import java.util.LinkedList;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;
import gameplay.Board;
import view.Menu;

/**
 *
 * @author lucas
 */
public class ClassicSnake extends JPanel{
    Image bodyImage;
    Image headImage;
        private final static int allBody = 1500;
    private int headWidth;
    private int headHeigth;
    protected final static int x[] = new int[allBody];
    protected final static int y[] = new int[allBody];
    private static int bodySegments = 15;
    private static int bodySize;
    
    private boolean leftDirection = false;
    private boolean rightDirection = true;
    private boolean upDirection = false;
    private boolean downDirection = false;
    //protected boolean running = true;
    private Board board;
    private SimpleFruit fruit;
    //private Gameplay gameplay;
   
  
    private int z;
    
    public ClassicSnake(){  
        //fruit = new SimpleFruit();
        loadImage();
        initSnakePos();
    }
    protected void loadImage() {
        fruit = new SimpleFruit();
        ImageIcon iiBody = new ImageIcon("src/img/greenSnake.png");
        bodyImage = iiBody.getImage();
        ImageIcon iiHead = new ImageIcon("src/img/snakeHead.png");
        headImage = iiHead.getImage();
    }
    public void initSnakePos() {
        bodySize = 3;

        for (z = 0; z < bodySize; z++) {
            x[z] = 75 - z * 12;
            y[z] = 75;
        }
       
    }
    public boolean checkAteFruit(){
        if ((x[0] == SimpleFruit.getFruit_x()) && (y[0] == SimpleFruit.getFruit_y())) {
            if(FruitGenerator.getGeneratedFruitID()==2){
              
                setBodySize(3);
            }
            if(FruitGenerator.getGeneratedFruitID()==3){
                Gameplay.gameOver();
            }
            Gameplay.increasePoints();
            Gameplay.getScoreLabel().setText("Score: "+Gameplay.getPoints());
            increaseBody();     
            SimpleFruit.generateFruit();      
            return true;
        }
        return false;
    }

    public void move() {

        for (z = bodySize; z > 0; z--) {
            x[z] = x[(z - 1)];
            y[z] = y[(z - 1)];
        }

        if (leftDirection) {
            x[0] -= bodySegments;
        }

        if (rightDirection) {
            x[0] += bodySegments;
        }

        if (upDirection) {
            y[0] -= bodySegments; 
        }

        if (downDirection) {
            y[0] += bodySegments;
        }
    }
    public void checkCollision() {

        for (int z = bodySize; z > 0; z--) {

        if ((z > 4) && (x[0] == x[z]) && (y[0] == y[z])) {
            Gameplay.gameOver();
        }
        }

        if (y[0] >= board.getBoardHeight()) {
            Gameplay.gameOver();
        }

        if (y[0] < 0) {
            Gameplay.gameOver();
        }

        if (x[0] >= board.getBoardWidth()) {
            Gameplay.gameOver();
        }

        if (x[0] < 0) {
            Gameplay.gameOver();
        }
        if(Menu.getChosenSnake()!=2){
            if(x[0]>=195 && x[0]<340 && y[0]==300){
                Gameplay.gameOver();
            }

            if(x[0]>=195 && x[0]<=340 && y[0]==195){
                Gameplay.gameOver();
            } 

            if(x[0]==105 && y[0]>=90 && y[0]<=240){
                Gameplay.gameOver();
            }
        }
        
    }
    
    public int increaseBody(){
        return bodySize++;     
    }
    
     public static int reduceBody(){
        return bodySize=3;     
    }
    
     

   

    public Image getBodyImage() {
        return bodyImage;
    }

    public Image getHeadImage() {
        return headImage;
    }

    public int getAllBody() {
        return allBody;
    }

    public static int getBodySegments() {
        return bodySegments;
    }

    public int getBodySize() {
        return bodySize;
    }

    public int[] getDX() {
        return x;
    }

    public int[] getDY() {
        return y;
    }

    public boolean getLeftDirection() {
        return leftDirection;
    }

    public void setLeftDirection(boolean leftDirection) {
        this.leftDirection = leftDirection;
    }

    public boolean getRightDirection() {
        return rightDirection;
    }

    public void setRightDirection(boolean rightDirection) {
        this.rightDirection = rightDirection;
    }

    public boolean getUpDirection() {
        return upDirection;
    }

    public void setUpDirection(boolean upDirection) {
        this.upDirection = upDirection;
    }

    public boolean getDownDirection() {
        return downDirection;
    }

    public void setDownDirection(boolean downDirection) {
        this.downDirection = downDirection;
    }

    public int getZ() {
        return z;
    }

    public void setZ(int z) {
        this.z = z;
    }

    public void setBodySize(int bodySize) {
        this.bodySize = bodySize;
    }


   
}
