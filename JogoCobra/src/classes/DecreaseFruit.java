/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import static classes.SimpleFruit.fruitImage;
import javax.swing.ImageIcon;

/**
 *
 * @author lucas
 */
public class DecreaseFruit extends SimpleFruit{
    public DecreaseFruit(){
        super();
        loadImage();
        SimpleFruit.setBigFruit(false);
    }

    @Override
    protected void loadImage() {
        ImageIcon iiFruit = new ImageIcon("src/img/decreaseFruit.png");
        fruitImage = iiFruit.getImage();
        
    }
    
    
    
}
