/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import gameplay.FruitGenerator;
import gameplay.Gameplay;
import java.awt.Image;
import java.util.Random;
import javax.swing.ImageIcon;

/**
 *
 * @author lucas
 */



public class SimpleFruit {

   
    protected static Image fruitImage;
    private static int fruit_x;
    private static int fruit_y;
    private static int randPos=29;
    private int fruitPoint=1;
    private static boolean bigFruit;
   
    
    public SimpleFruit(){
        loadImage();
        generateFruit();      
        SimpleFruit.setBigFruit(false);
    }
    
    protected void loadImage(){
            ImageIcon iiFruit = new ImageIcon("src/img/simpleFruit.png");
        fruitImage = iiFruit.getImage();
    }
    
    
    public static void generateFruit(){ 
        Random random =new Random();
        //random = (int) (Math.random() * randPos);
        fruit_x = (random.nextInt(randPos)*15);
        System.out.println("Fruit X: "+SimpleFruit.getFruit_x());
        System.out.println("Fruit Y: "+SimpleFruit.getFruit_x());
        fruit_y = (random.nextInt(randPos)*15);
    }
    
    
     public static int getFruit_x() {
        return fruit_x;
    }

    public void setFruit_x(int fruit_x) {
        this.fruit_x = fruit_x;
    }

    public static int getFruit_y() {
        return fruit_y;
    }

    public void setFruit_y(int fruit_y) {
        this.fruit_y = fruit_y;
    }

    public static Image getFruitImage() {
        return fruitImage;
    }

    public void setFruitImage(Image fruitImage) {
        this.fruitImage = fruitImage;
    }

    public int getFruitPoint() {
        return fruitPoint;
    }

    public void setFruitPoint(int fruitPoint) {
        this.fruitPoint = fruitPoint;
    }

    public static boolean isBigFruit() {
        return bigFruit;
    }

    public static void setBigFruit(boolean aBigFruit) {
        bigFruit = aBigFruit;
    }

    
   
    
}
