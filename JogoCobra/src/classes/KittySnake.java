/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import static classes.ClassicSnake.x;
import static classes.ClassicSnake.y;
import gameplay.FruitGenerator;
import gameplay.Gameplay;
import javax.swing.ImageIcon;

/**
 *
 * @author lucas
 */
public class KittySnake extends ClassicSnake{
    public KittySnake(){
        super();
    }

    protected void loadImage() {
        ImageIcon iiBody = new ImageIcon("src/img/blueSnake.png");
        bodyImage = iiBody.getImage();
        ImageIcon iiHead = new ImageIcon("src/img/snakeHead.png");
        headImage = iiHead.getImage();
    }
    
    
    
    
}



