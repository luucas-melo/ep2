/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import gameplay.FruitGenerator;
import gameplay.Gameplay;
import java.awt.Graphics;
import java.awt.Image;
import javax.swing.ImageIcon;

/**
 *
 * @author lucas
 */
public class StarSnake extends ClassicSnake{
    
 
    public StarSnake() {
       super();
        
    }
    
    
    @Override
    protected void loadImage() {
        ImageIcon iiBody = new ImageIcon("src/img/yellowSnake.png");
        bodyImage = iiBody.getImage();
        ImageIcon iiHead = new ImageIcon("src/img/snakeHead.png");
        headImage = iiHead.getImage();
    }
    
    
    @Override
    public boolean checkAteFruit() {
         if ((x[0] == SimpleFruit.getFruit_x()) && (y[0] == SimpleFruit.getFruit_y())) {
             if(FruitGenerator.getGeneratedFruitID()==3){
                System.out.println("diminuiu");
                setBodySize(3);
            }
            if(FruitGenerator.getGeneratedFruitID()==2){
                System.out.println("morreu");
                Gameplay.gameOver();
            }
            Gameplay.increasePoints();
            Gameplay.getScoreLabel().setText("Score: "+Gameplay.getPoints()*2);
            increaseBody();
            SimpleFruit.generateFruit();
            return true;
        }
         
         return false;
    }

    

    
  
    
}
